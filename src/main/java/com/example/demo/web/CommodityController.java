package com.example.demo.web;

import com.example.demo.domain.Commodity;
import com.example.demo.domain.CommodityRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/commodities")
@CrossOrigin(origins = "*")
public class CommodityController {
    private CommodityRepository repository;

    public CommodityController(CommodityRepository repository) {
        this.repository = repository;
    }

    @GetMapping
    public ResponseEntity getAll() {
        List<Commodity> commodities = repository.findAll();
        System.out.println(repository.findAll());
        return ResponseEntity.ok(commodities);
    }

}
