CREATE TABLE IF NOT EXISTS orders
(
    id          BIGINT AUTO_INCREMENT,
    commodityId bigint,
    PRIMARY KEY (id)
);