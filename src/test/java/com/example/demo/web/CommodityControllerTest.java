package com.example.demo.web;

import com.example.demo.domain.Commodity;
import com.example.demo.domain.CommodityRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CommodityControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private CommodityRepository commodityRepository;

    @Test
    void should_get_commodities() throws Exception {
        Commodity commodity = new Commodity("cola", 3.00, "bottle", "location");
        commodityRepository.save(commodity);
        mockMvc.perform(get("/api/commodities"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].name").value("cola"))
                .andExpect(jsonPath("$[0].price").value(3.00))
                .andExpect(jsonPath("$[0].unit").value("bottle"))
                .andExpect(jsonPath("$[0].image").value("location"));
    }


}