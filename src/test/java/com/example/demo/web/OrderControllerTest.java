package com.example.demo.web;

import com.example.demo.domain.Commodity;
import com.example.demo.domain.CommodityRepository;
import com.example.demo.domain.Order;
import com.example.demo.domain.OrderRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class OrderControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private CommodityRepository commodityRepository;

    @Test
    void should_get_commodities() throws Exception {

        Commodity saveCommodity = commodityRepository.saveAndFlush(new Commodity("cola", 3.00, "bottle", "location"));
        Order order = new Order(saveCommodity);
        orderRepository.saveAndFlush(order);
        mockMvc.perform(get("/api/orders"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$[0].commodity.name").value("cola"))
                .andExpect(jsonPath("$[0].commodity.price").value(3.00))
                .andExpect(jsonPath("$[0].commodity.unit").value("bottle"))
                .andExpect(jsonPath("$[0].commodity.image").value("location"));
    }


}